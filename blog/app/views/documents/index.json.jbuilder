json.array!(@documents) do |document|
  json.extract! document, :id, :filename, :content_type, :file
  json.url document_url(document, format: :json)
end
