class ProductsController < ApplicationController
  def create
    @post_attachment = PostAttachment.find(params[:post_attachment_id])
    @product = @post_attachment.products.create(product_params)
    redirect_to post_attachment_path(@post_attachment)
  end

  private
  def product_params
    params.require(:product).permit(:name, :description, :cost, :stock)
  end
end
