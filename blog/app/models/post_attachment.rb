class PostAttachment < ActiveRecord::Base
  has_many :products
  mount_uploader :avatar, AvatarUploader
  belongs_to :post
end
