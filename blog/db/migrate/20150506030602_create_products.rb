class CreateProducts < ActiveRecord::Migration
  def change
    create_table :products do |t|
      t.string :name
      t.text :description
      t.text :cost
      t.integer :stock
      t.references :post_attachment, index: true

      t.timestamps null: false
    end
    add_foreign_key :products, :post_attachments
  end
end
