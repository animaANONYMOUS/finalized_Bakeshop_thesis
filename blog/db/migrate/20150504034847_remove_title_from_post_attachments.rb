class RemoveTitleFromPostAttachments < ActiveRecord::Migration
  def change
    remove_column :post_attachments, :title, :string
  end
end
